//
//  CustomViewCell.m
//  Test2
//
//  Created by Ionut Dobrinescu on 18/04/2019.
//  Copyright © 2019 ionutdobrinescu. All rights reserved.
//

#import "CustomViewCell.h"

@implementation CustomViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = UIColor.clearColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

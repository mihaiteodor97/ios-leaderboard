//
//  AppDelegate.h
//  Test2
//
//  Created by Ionut Dobrinescu on 04/04/2019.
//  Copyright © 2019 ionutdobrinescu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


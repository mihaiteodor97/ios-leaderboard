//
//  PlayerClass.h
//  Test2
//
//  Created by Ionut Dobrinescu on 18/04/2019.
//  Copyright © 2019 ionutdobrinescu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PlayerClass : NSObject
@property(strong, nonatomic) NSString *name;
@property(nonatomic) int score;
@end

NS_ASSUME_NONNULL_END

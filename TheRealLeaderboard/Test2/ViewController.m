//
//  ViewController.m
//  Test2
//
//  Created by Ionut Dobrinescu on 04/04/2019.
//  Copyright © 2019 ionutdobrinescu. All rights reserved.
//


/*
 TO DO: ADD IMAGE TO PLAYER
 ADD SWIPE GESTURE
 */

#import "ViewController.h"
#import "CustomViewCell.h"
#import "PlayerClass.h"

@interface ViewController () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(strong,nonatomic) NSArray<NSString*> *myArray;
@property(strong,nonatomic) NSArray<NSString*> *myIndexArray;

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *forwardButton;
@property (weak, nonatomic) IBOutlet UILabel *monthLabel;


@property (nonatomic) int currentIndexPage;

@property (strong, nonatomic) NSDictionary<NSString*, NSArray<PlayerClass*>*> *playersData;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.currentIndexPage = 0;
    
    self.tableView.backgroundColor = UIColor.clearColor;
    
    self.populatePlayerDictionar;
    
    self.tableView.delegate =self;
    self.backButton.layer.cornerRadius = self.backButton.bounds.size.height/4;
    self.forwardButton.layer.cornerRadius = self.forwardButton.bounds.size.height/4;
    
    UIImage *sourceImage = [UIImage imageNamed:@"arrow.png"];
    UIImage *flippedImage = [UIImage imageWithCGImage:sourceImage.CGImage scale:sourceImage.scale orientation:UIImageOrientationUpMirrored];
    //self.forwardButton.imageView.image = flippedImage;
    [self.forwardButton setImage:flippedImage forState:UIControlStateNormal];
    
}

-(void)populatePlayerDictionar{
    NSArray<PlayerClass*> *playerArray;
    NSArray<PlayerClass*> *playerArray2;
    
    self.myIndexArray = @[@"Ianuarie", @"Februarie"];
    
    
    PlayerClass *player1 = [[PlayerClass alloc] init];
    
    player1.name = @"Frank";
    player1.score = 100;
    
    PlayerClass *player2 = [[PlayerClass alloc] init];
    player2.name = @"Joji";
    player2.score = 300;
    
    PlayerClass *player3 = [[PlayerClass alloc] init];
    player3.name = @"Chin Chin";
    player3.score = 210;
    
    PlayerClass *player4 = [[PlayerClass alloc] init];
    player4.name = @"Prometheus";
    player4.score = 150;
    
    PlayerClass *player5 = [[PlayerClass alloc] init];
    player5.name = @"Pink Guy";
    player5.score = 800;
    
    PlayerClass *player6 = [[PlayerClass alloc] init];
    player6.name = @"Salamander Man";
    player6.score = 101;
    
    playerArray = @[player1, player2, player3];
    playerArray2 = @[player4, player5, player6];
    
    self.playersData = @{@"Ianuarie" : playerArray, @"Februarie" : playerArray2};
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSString *currentMonth = self.myIndexArray[self.currentIndexPage];
    self.monthLabel.text = currentMonth;
    return self.playersData[currentMonth].count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CustomViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    NSString *currentMonth = self.playersData.allKeys[self.currentIndexPage];
    
    cell.indexLabel.text = [NSString stringWithFormat:@"%ld",indexPath.row];
    PlayerClass *player = [self.playersData[currentMonth] objectAtIndex:indexPath.row];
    cell.nameLabel.text = player.name;
    cell.pointsLabel.text = [NSString stringWithFormat:@"%d", player.score];
    
    
    return cell;
}

- (IBAction)backButtonPressed:(id)sender {
    if(self.currentIndexPage > 0)
    {
        self.currentIndexPage--;
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationLeft];
        [self.tableView reloadData];
    }
}

- (IBAction)forwardButtonPressed:(id)sender {
    if(self.currentIndexPage < self.playersData.allKeys.count - 1)
    {
        self.currentIndexPage++;
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationRight];
        [self.tableView reloadData];
    }
}



@end

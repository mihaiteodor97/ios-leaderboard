//
//  main.m
//  Test2
//
//  Created by Ionut Dobrinescu on 04/04/2019.
//  Copyright © 2019 ionutdobrinescu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
